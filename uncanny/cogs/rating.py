import logging
import random
from disnake.ext import commands

emotes = {
    # First, the "uncanny" phases
    "canny": "<:canny:1114522196442890260>",
    "mild": "<:mild:1114522227312959577>",
    "startling": "<:startling:1114522240306909266>",
    "scary": "<:scary:1114522231465316383>",
    "hollow": "<:hollow:1114522222233661440>",
    "creepy": "<:creepy:1114522202931478588>",
    "uncanny": "<:uncanny:1114522242144018493>",
    "dark": "<:dark:1114522205674545172>",
    "dead": "<:dead:1114522207360667771>",
    "despair": "<:despair:1114522210841923704>",

    # Then the "becoming canny" phases
    "shining": "<:shining:1114522235911290972>",
    "ecstatic": "<:ecstatic:1114522217968054363>",
    "cool": "<:cool2:1114522199840272494>",
    "devious": "<:devious:1114522215023661126>",
    "ascended": "<:ascended:1114522194412830760>",

    # Weird phases
    "mario": "<:mario:1114531968948916305>",
    "australian": "<:australian:1277896224019578991>",
    "clueless": "<:clueless:1277896226259472384>",
    "emo": "<:emo:1277896229371646019>",
    "fucked up evil": "<:evil:1277896231779041342>",
    "gone": "<:gone:1277896233389916213>",
    "lesbian": "<:lesbian:1277896234862116905>",
    "luigi": "<:luigi:1277896236497899530>",
    "michiru": "<:michiru:1277896238112571423>",
    "soy": "<:soy:1277896239429455915>",
    "under construction": "<:constructionwork:1277896227882799187>"
}

class Rating(commands.Cog):
    def __init__(self):
        self.l = logging.getLogger("uncanny.cogs.rating")

    @commands.slash_command(description="Roll the Uncanny dice!")
    async def roll(self, inter):
        level, emote = random.choice(list(emotes.items()))
        await inter.send(f"your rating is: damn **{level}** {emote}")
        self.l.info(f"{inter.author} rolled a '{level}'")

    @commands.slash_command(description="Get some info")
    async def help(self, inter):
        msg = """
Hello, it is I, Mr. Incredible from such classics as Pixar's 'Incredibles' and the 'Mr. Incredible Becomes (Un)Canny' meme!
You can use `/roll` for me to randomly rate whatever is happening. Link to the server with emotes is in my About Me."""
        await inter.send(msg, ephemeral=True)
