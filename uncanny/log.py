# Code very kindly borrowed from https://codeberg.org/FDL/puffer 
import logging
from colorama import Fore
from colorama import init as init_clrm

init_clrm()


class CustomFormatter(logging.Formatter):
    FORMAT = (
        "[%(asctime)s] [%(levelname)s] %(message)s (%(filename)s:%(lineno)d)"
    )

    COLORS = {
        logging.DEBUG: Fore.WHITE,
        logging.INFO: Fore.LIGHTBLUE_EX,
        logging.WARNING: Fore.YELLOW,
        logging.ERROR: Fore.LIGHTRED_EX,
        logging.CRITICAL: Fore.RED,
    }

    def format(self, record):
        fmt = self.COLORS[record.levelno] + self.FORMAT + Fore.RESET
        formatter = logging.Formatter(fmt)
        return formatter.format(record)


def setup_logging():
    init_clrm()
    root = logging.getLogger()
    root.setLevel(logging.WARNING)
    handler = logging.StreamHandler()
    handler.setFormatter(CustomFormatter())
    root.addHandler(handler)

    logging.getLogger("uncanny").setLevel(logging.DEBUG)
