import logging, disnake
from disnake.ext.commands import InteractionBot
import uncanny

uncanny.log.setup_logging()

intents: disnake.Intents = disnake.Intents.default()
bot = InteractionBot(intents=intents)

# smart cog loading, borrowed from puffer again
def load_cog(cls, *args, **kwargs):
    bot.add_cog(cls(*args, **kwargs))

# cog loads
load_cog(uncanny.cogs.Rating)

@bot.event
async def on_ready():
    game = disnake.Game("with The Sinister Potion")
    await bot.change_presence(activity=game)
    logging.getLogger("uncanny").info(f"Launched as {bot.user}")

bot.run(uncanny.env.TOKEN)

