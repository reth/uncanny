# Mr. Incredible Bot
the uncanny bot.

i have no idea what to write about here uhhhh i mean, you can roll a dice with `/roll`, the bot will give you the "uncanniness rating", which is just rolled randomly.

bot invite: [link](https://discord.com/api/oauth2/authorize?client_id=1114501681535783013&permissions=264192&scope=bot%20applications.commands)  
discord w/ emotes: [link](https://discord.gg/pKEzJTbBzp)

no idea if i will ever update this bot with features, time will tell.

## how to host an instance for yourself
first, install python-poetry (google it), then clone this repo and do `poetry install` to get the deps.

create the file `uncanny/token.py`, in it write the following:
```py
TOKEN = "your discord bot token"
```
insert your token where required, then launch the bot with `python -m uncanny`. should go well.